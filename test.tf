#provider details
provider "aws" {
  version = "~> 2.0"
  region  = "eu-central-1"
  profile = "airbox-sandbox"
}

resource "aws_instance" "example" {
  ami           = "ami-09356619876445425"
  instance_type = "t3.micro"
  user_data = "${file("init.yaml")}"
  associate_public_ip_address = true
  security_groups = ["${aws_security_group.security_group_ec2.name}"]
 

}

module "vpc" {
  source = "git::https://bitbucket.org/oaugustas/terraform-aws-vpc.git"
  name = "my-vpc"
  cidr = "10.0.0.0/16"
  azs             = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  enable_nat_gateway = false
  enable_vpn_gateway =false
  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
resource "aws_security_group" "security_group_ec2" {
name = "allow_ssh"
ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }
// Terraform removes the default rule
  egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}